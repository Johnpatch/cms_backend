var path = require('path');

var config = {
    debug: true,
	mysql : {
		host : '127.0.0.1',
		user : 'root',
        password : '',
		database : 'storeapp'
	},
	port: 5010,
	server: '192.168.8.55',
	smtp: {
		host: 'smtp.gmail.com',
		port: 465,
		user: '',
		pass: ''
	}
}

module.exports = config