var express = require('express');
var router = express.Router();

var model = require('./model/app');

router.post('/registerUser', async function (req, res) {
    
    var result = await model.registerUser(req.body)
    return res.json({
        code: result ? 20000 : 60000,
        message: result ? '' : 'Failed',
        data: null
    })
})


router.post('/getSlide', async function (req, res) {
    // const { page, limit } = req.body
    var data = await model.getSlide(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});

router.post('/HFImage', async function (req, res) {
    var data = await model.HFImage(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/checkMenuType', async function (req, res) {
    var data = await model.checkMenuType(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/getTopMenu', async function (req, res) {
    var data = await model.getTopMenu(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/getNews', async function (req, res) {
    var data = await model.getNews(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/deviceExist', async function (req, res) {
    var data = await model.deviceExist(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/getLatLng', async function (req, res) {
    var data = await model.getLatLng(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/getWebviewList', async function (req, res) {
    var data = await model.getWebviewList(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/getPhotoVideo', async function (req, res) {
    var data = await model.getPhotoVideo(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/getCompanyProfile', async function (req, res) {
    var data = await model.getCompanyProfile(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/getStoreInformation', async function (req, res) {
    var data = await model.getStoreInformation(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/getFacebook', async function (req, res) {
    var data = await model.getFacebook(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/getInstagram', async function (req, res) {
    var data = await model.getInstagram(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/getTwitter', async function (req, res) {
    var data = await model.getTwitter(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/getLayoutSetting', async function (req, res) {
    var data = await model.getLayoutSetting(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/getPhilosophy', async function (req, res) {
    var data = await model.getPhilosophy(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/getIntroduction', async function (req, res) {
    var data = await model.getIntroduction(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/getHistory', async function (req, res) {
    var data = await model.getHistory(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/getCatalogue', async function (req, res) {
    var data = await model.getCatalogue(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/getGallery', async function (req, res) {
    var data = await model.getGallery(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/getStaff', async function (req, res) {
    var data = await model.getStaff(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/getReservation', async function (req, res) {
    var data = await model.getReservation(req.body)
    return res.json({
        code: 20000,
        data: data
    })
});
router.post('/addReservation', async function (req, res) {
    
    var result = await model.addReservation(req.body)
    return res.json({
        code: result ? 20000 : 60000,
        message: result ? '' : 'Failed',
        data: null
    })
})
router.post('/cancelReservation', async function (req, res) {
    
    var result = await model.cancelReservation(req.body)
    return res.json({
        code: result ? 20000 : 60000,
        message: result ? '' : 'Failed',
        data: null
    })
})
router.post('/sendPush', async function (req, res) {
    await model.sendPush()
    return res.json({
        code: 20000,
        message: '',
        data: null
    })
})
router.post('/sendBirthPush', async function (req, res) {
    await model.sendPush()
    return res.json({
        code: 20000,
        message: '',
        data: null
    })
})
router.post('/sendWelcomePush', async function (req, res) {
    await model.sendPush()
    return res.json({
        code: 20000,
        message: '',
        data: null
    })
})
router.post('/getCoupon', async function (req, res) {
    var data = await model.getCoupon(req.body)
    return res.json({
        code: 20000,
        data: data
    })
})

router.post('/getStamp', async function (req, res) {
    var data = await model.getStamp(req.body)
    return res.json({
        code: 20000,
        data: data
    })
})

module.exports = router;