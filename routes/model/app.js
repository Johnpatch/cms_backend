var db = require('../../utils/database');
var md5 = require('md5');
//var adminUserModel = require('./admin_user');
var request = require('request');
var moment = require('moment');
var config = require('../../src/config');
var dateFormat = require('dateformat');

var registerUser = function (params) {
    var curtime = Date.now()
    
    var insert_fields = [
        'USER_ID', 'NICK_NAME', 'BIRTHDAY', 'SEX', 'DEVICE_ID', 'DEVICE_TYPE', 'CREATE_TIME', 'UPDATE_TIME', 'PUSH_TOKEN', 'BRANCH_ID'
    ]
    for (var i = 0; i < insert_fields.length; i++) {
        insert_fields[i] = '`' + insert_fields[i] + '`'
    }
    
    var insert_vals = [
        "'" + params['data']['user_id'] + "'",
        "'" + params['data']['nick_name'] + "'",
        "'" + params['data']['birthday'] + "'",
        "'" + params['data']['sex'] + "'",
        "'" + params['data']['device_id'] + "'",
        "'" + params['data']['device_type'] + "'",
        "'" + curtime + "'",
        "'" + curtime + "'",
        "'" + params['data']['token'] + "'",
        "'" + params['data']['branch_id']+"'"
    ]
    
    db.cmd(db.statement('insert into', 'users', '(' + insert_fields.join(',') + ')', '', 'VALUES (' + insert_vals.join(',') + ')'))
    return true
}

var getSlide = function (params) {
    var curtime = dateFormat(new Date(), 'yyyy-mm-dd HH:mm:ss')

    var whereItems = []
    
    whereItems.push({
        key: 'STATUS_SHOW',
        val: 'Y'
    })

    whereItems.push({
        key: 'VIEW_START_DATE',
        val: curtime,
        opt : '<='
    })

    whereItems.push({
        key: 'VIEW_END_DATE',
        val: curtime,
        opt : '>='
    })
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select * from", "slide_settings", "", whereClause, ''), true)
    
}

var HFImage = function (params) {
    var whereItems = []
    console.log(params);
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select * from", "header_footer_settings", "", whereClause, ''), true)
}

var checkMenuType = function (params) {
    var whereItems = []
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select LAYOUT_TYPE from", "layout_settings", "", whereClause, ''), true)
}

var getTopMenu = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'STATUS_SHOW',
        val: 'Y'
    })

    if(params['data']['os'] == 'android')
        whereItems.push({
            key: 'ANDROID_YN',
            val: 'Y',
        })
    if(params['data']['os'] == 'ios')
        whereItems.push({
            key: 'IOS_YN',
            val: 'Y',
        })
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    
    var whereClause = db.lineClause(whereItems, 'and')

    return db.list(db.statement("select * from", "topmenu_settings", "", whereClause, 'ORDER BY `ORDER` asc'), true)
}

var getNews = function (params) {
    var curtime = dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss")

    var whereItems = []
    
    whereItems.push({
        key: 'PUBLIC_PERIOD_SETTING',
        val: 'Y'
    })

    /*whereItems.push({
        key: 'PUBLIC_TYPE',
        val: '1'
    })*/
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    
    var whereClause = db.lineClause(whereItems, 'and')

    return db.list(db.statement("select * from", "events", "", whereClause, 'ORDER BY `ORDER` asc'), true)
}

var deviceExist = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'DEVICE_ID',
        val: params['data']['deviceID']
    })
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')

    return db.list(db.statement("select * from", "users", "", whereClause, ''), true)
}

var getLatLng = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')

    return db.list(db.statement("select LAT,LNG from", "branches", "", whereClause, ''), true)
}

var getWebviewList = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'IS_SHOW',
        val: 1
    })
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select * from", "webview_settings", "", whereClause, ''), true)
}

var getPhotoVideo = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select * from", "photo_video_info", "", whereClause, ''), true)
}
var getCompanyProfile = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select * from", "company_profile", "", whereClause, ''), true)
}
var getStoreInformation = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select * from", "branches", "", whereClause, ''), true)
}
var getFacebook = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select * from", "facebook", "", whereClause, ''), true)
}
var getInstagram = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select * from", "instagram", "", whereClause, ''), true)
}
var getTwitter = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select * from", "twitter", "", whereClause, ''), true)
}
var getLayoutSetting = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select * from", "layout_settings", "", whereClause, ''), true)
}
var getPhilosophy = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select * from", "philosophy", "", whereClause, ''), true)
}
var getIntroduction = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select * from", "introduction", "", whereClause, ''), true)
}
var getHistory = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select * from", "business_history", "", whereClause, ''), true)
}
var getCatalogue = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select * from", "catalogs", "", whereClause, ''), true)
}
var getGallery = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'CATALOG_ID',
        val: params['data']['ID']
    })
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select * from", "catalog_photos", "", whereClause, ''), true)
}
var getStaff = function (params) {
    var whereItems = []
   
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select * from", "staff", "", whereClause, ''), true)
}
var getReservation = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'USER_ID',
        val: params['data']['user_id']
    })
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select * from", "reservation", "", whereClause, 'ORDER BY RESERVATION_DATETIME DESC'), true)
}
var addReservation = function (params) {
    var curtime = Date.now()
    
    var insert_fields = [
        'BRANCH_ID', 'USER_ID', 'NUMBER_OF_RPERSONS', 'RESERVATION_DATETIME', 'STATUS', 'CREATE_TIME', 'UPDATE_TIME'
    ]
    for (var i = 0; i < insert_fields.length; i++) {
        insert_fields[i] = '`' + insert_fields[i] + '`'
    }
    var insert_vals = [
        "'" + params['data']['branch_id'] +"'",
        "'" + params['data']['user_id'] +"'",
        "'" + params['data']['number'] + "'",
        "'" + moment(params['data']['date']).format('YYYY') + "-"+ moment(params['data']['date']).format('MM')+"-"+moment(params['data']['date']).format('DD')+" "+moment(params['data']['time']).format('HH')+":"+moment(params['data']['time']).format('mm')+"'",
        "'1'",
        "'" + curtime + "'",
        "'" + curtime + "'"
    ]
    
    db.cmd(db.statement('insert into', 'reservation', '(' + insert_fields.join(',') + ')', '', 'VALUES (' + insert_vals.join(',') + ')'))
    return true
}
var cancelReservation = function (params) {
    var curtime = Date.now()
    
    const id = params['data']['id']
    
    if (id === undefined || id == 0) {
        return false
    } else {
        var whereItems = []
        whereItems.push(
            {
                key: 'ID',
                val: id
            }
        )
        whereClause = db.lineClause(whereItems, 'and')

        var setItems = []

        setItems.push({
            key: 'STATUS',
            val: 2
        })
        setItems.push({
            key: 'UPDATE_TIME',
            val: curtime
        })

        db.cmd(db.statement('update', 'reservation', 'set ' + db.lineClause(setItems, ','), whereClause))
        return true
    }
}

var sendPush = async function () {
    var curtime = dateFormat(new Date(), 'yyyy-mm-dd HH:mm:ss')
    var whereItems = []
    whereItems.push(
        {
            key: 'DELIVERY_DATETIME',
            val: curtime,
            opt: '>='
        }
    )
    whereItems.push(
        {
            key: 'DELIVERY_DATETIME',
            val: curtime,
            opt: '<='
        }
    )
    whereClause = db.lineClause(whereItems, 'and')

    return db.list(db.statement("select * from ", "push_notification", "", whereClause), true).then((rows) => {
        if(rows.length > 0){
            var whereItems1 = []
            var whereItems2 = []
            var whereClause1;
            var whereClause2;
            whereItems1.push(
                {
                    key: 'BRANCH_ID',
                    val: rows[i]['BRANCH_ID'],
                }
            )
            whereItems1.push(
                {
                    key: 'USER_TYPE',
                    val: 2,
                }
            )
            for(var i = 0; i< rows.length; i++){
                if(rows[i]['DELIVERY_TYPE'] == 1 || rows[i]['DELIVERY_TYPE'] == 2){
                    if(rows[i]['DELIVERY_TARGET'] == 1)
                        whereClause1 = db.lineClause(whereItems1, 'and')
                    else if(rows[i]['DELIVERY_TARGET'] == 2){
                        if(rows[i]['SEX'] == 1)
                            whereItems1.push(
                                {
                                    key: 'SEX',
                                    val: 'M',
                                }
                            )
                        if(rows[i]['SEX'] == 2)
                            whereItems1.push(
                                {
                                    key: 'SEX',
                                    val: 'F',
                                }
                            )
                        var date = new Date();
                        var start_birth = date.getFullYear() - rows[i]['AGE_FROM']+"-"+(date.getMonth()+1)+"-"+date.getDate();
                        var end_birth = date.getFullYear() - rows[i]['AGE_TO']+"-"+(date.getMonth()+1)+"-"+date.getDate();
                        if(start_birth > end_birth){
                            whereItems1.push(
                                {
                                    key: 'BIRTHDAY',
                                    val: start_birth,
                                    opt : '<='
                                }
                            )
                            whereItems1.push(
                                {
                                    key: 'BIRTHDAY',
                                    val: end_birth,
                                    opt : '>='
                                }
                            )
                        }
                        else{
                            whereItems1.push(
                                {
                                    key: 'BIRTHDAY',
                                    val: start_birth,
                                    opt : '>='
                                }
                            )
                            whereItems1.push(
                                {
                                    key: 'BIRTHDAY',
                                    val: end_birth,
                                    opt : '<='
                                }
                            )
                        }
                        
                        whereClause1 = db.lineClause(whereItems1, 'and')
                    }
                    
                    whereItems2.push(
                        {
                            key: 'ID',
                            val: rows[i]['REFER_ID'],
                        }
                    )
                    whereClause2 = db.lineClause(whereItems2, 'and')
                    var body = {
                        to: '',
                        sound: 'default',
                        title: '',
                        body: ''
                    }
                    var table_name = ''
                    if(rows[i]['DELVIERY_TYPE'] == 1){
                        table_name = 'events'
                    } else if(rows[i]['DELIVERY_TYPE'] == 2){
                        table_name = 'coupon'
                    }
                    db.list(db.statement("select * from ", table_name, "", whereClause2), true).then((rows2) => {
                        if(rows2.length > 0){
                            body['title'] = rows2[0]['TITLE']
                            db.list(db.statement("select * from ", "users", "", whereClause1), true).then((rows3) => {
                                if(rows3.length > 0){
                                    for(var j = 0;j<rows3.length;j++){
                                        body['to'] = rows3[j]['PUSH_TOKEN'];
                                        var options = {
                                            'method': 'POST',
                                            'url': 'https://exp.host/--/api/v2/push/send',
                                            'headers': {
                                                'Accept': 'application/json',
                                                'Content-Type': 'application/json'
                                            },
                                            body: JSON.stringify(body)
                                        };
                                        request(options, function (error, response) {});
                                    }
                                    
                                }
                            })
                        }
                    })
                }
                
            }
        }else{
            return null;
        }
    })
}

var sendBirthPush = async function () {
    
    var whereItems = []
    var whereItems1 = []
    whereItems.push(
        {
            key: 'AUTO_PUSH_YN',
            val: 1,
        }
    )
    whereItems.push(
        {
            key: 'PUSH_TYPE',
            val: 1,
        }
    )
    whereClause = db.lineClause(whereItems, 'and')
    var body = {
        to: '',
        sound: 'default',
        title: '',
        body: ''
    }
    return db.list(db.statement("select * from ", "birthday_welcome_push", "", whereClause), true).then((rows) => {
        if(rows.length > 0){
            for(var i = 0; i<rows.length;i++){
                body['title'] = rows[i]['TITLE'];
                body['body'] = rows[i]['DETAIL'];
                var beforedate = new Date();
                if(rows[i]['DEVLEIRY_DATE'] == 1)
                    curtime = dateFormat(new Date().setDate(beforedate.getDate()-30), 'yyyy-mm-dd');
                else if(rows[i]['DEVLEIRY_DATE'] == 2)
                    curtime = dateFormat(new Date().setDate(beforedate.getDate()-7), 'yyyy-mm-dd');
                else if(rows[i]['DEVLEIRY_DATE'] == 2)
                    curtime = dateFormat(new Date().setDate(beforedate.getDate()-1), 'yyyy-mm-dd');
                else if(rows[i]['DEVLEIRY_DATE'] == 4)
                    curtime = dateFormat(new Date(), 'yyyy-mm-dd');
                whereItems1.push(
                    {
                        key: 'BRANCH_ID',
                        val: rows[i]['BRANCH_ID'],
                    }
                )
                whereItems1.push(
                    {
                        key: 'BIRTHDAY',
                        val: curtime,
                    }
                )
                whereClause1 = db.lineClause(whereItems1, 'and')
                db.list(db.statement("select * from ", "users", "", whereClause1), true).then((rows1) => {
                    if(rows1.length > 0){
                        for(var j =0;j<rows1.length;j++){
                            body['to'] = rows1[j]['PUSH_TOKEN'];
                            var options = {
                                'method': 'POST',
                                'url': 'https://exp.host/--/api/v2/push/send',
                                'headers': {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json'
                                },
                                body: JSON.stringify(body)
                            };
                            request(options, function (error, response) {});
                        }
                        
                    }
                })
            }
        }else{
            return null;
        }
    })
}

var sendWelcomePush = async function () {
    
    var whereItems = []
    var whereItems1 = []
    whereItems.push(
        {
            key: 'AUTO_PUSH_YN',
            val: 1,
        }
    )
    whereItems.push(
        {
            key: 'PUSH_TYPE',
            val: 2,
        }
    )
    whereClause = db.lineClause(whereItems, 'and')
    var body = {
        to: '',
        sound: 'default',
        title: '',
        body: ''
    }
    return db.list(db.statement("select * from ", "birthday_welcome_push", "", whereClause), true).then((rows) => {
        if(rows.length > 0){
            for(var i = 0; i<rows.length;i++){
                body['title'] = rows[i]['TITLE'];
                body['body'] = rows[i]['DETAIL'];
                var curtime = Date.now();
                var curtime1 = Date.now();
                if(rows[i]['DEVLEIRY_DATE'] == 1){
                    curtime = curtime - 30 * 60 * 1000;
                    curtime1 = curtime1 - 29 * 60 * 1000;
                }
                else if(rows[i]['DEVLEIRY_DATE'] == 2){
                    curtime = curtime - 120 * 60 * 1000;
                    curtime1 = curtime1 - 119 * 60 * 1000;
                }
                else if(rows[i]['DEVLEIRY_DATE'] == 3){
                    curtime = curtime - 1440 * 60 * 1000;
                    curtime1 = curtime1 - 1439 * 60 * 1000;
                }
                else if(rows[i]['DEVLEIRY_DATE'] == 4){
                    curtime = curtime - 2880 * 60 * 1000;
                    curtime1 = curtime1 - 2879 * 60 * 1000;
                }
                whereItems1.push(
                    {
                        key: 'BRANCH_ID',
                        val: rows[i]['BRANCH_ID'],
                    }
                )
                whereItems1.push(
                    {
                        key: 'CREATE_TIME',
                        val: curtime,
                        opt: '<='
                    }
                )
                whereItems1.push(
                    {
                        key: 'CREATE_TIME',
                        val: curtime1,
                        opt: '>='
                    }
                )
                whereClause1 = db.lineClause(whereItems1, 'and')
                db.list(db.statement("select * from ", "users", "", whereClause1), true).then((rows1) => {
                    if(rows1.length > 0){
                        for(var j =0;j<rows1.length;j++){
                            body['to'] = rows1[j]['PUSH_TOKEN'];
                            var options = {
                                'method': 'POST',
                                'url': 'https://exp.host/--/api/v2/push/send',
                                'headers': {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json'
                                },
                                body: JSON.stringify(body)
                            };
                            request(options, function (error, response) {});
                        }
                        
                    }
                })
            }
        }else{
            return null;
        }
    })
}
var getCoupon = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'BRANCH_ID',
        val: params['data']['branch_id']
    })
    whereItems.push({
        key: 'COUPON_START_DATE',
        val: params['data']['cur_time'],
        opt : '<='
    })
    whereItems.push({
        key: 'COUPON_END_DATE',
        val: params['data']['cur_time'],
        opt : '>='
    })
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select * from", "coupon", "", whereClause, 'ORDER BY `ORDER` asc'), true)
}
var getStamp = function (params) {
    var whereItems = []
    
    whereItems.push({
        key: 'stamp_info.BRANCH_ID',
        val: params['data']['branch_id']
    })
    
    var whereClause = db.lineClause(whereItems, 'and')
    return db.list(db.statement("select stamp_info.*, stamp_image.URL from", "stamp_info", " inner join stamp_image ON stamp_image.ID = stamp_info.STAMP_IMAGE ", whereClause, 'ORDER BY `ID` desc'), true)
}
var model = {
    registerUser: registerUser,
    getSlide: getSlide,
    HFImage: HFImage,
    checkMenuType: checkMenuType,
    getTopMenu : getTopMenu,
    getNews : getNews,
    deviceExist : deviceExist,
    getLatLng: getLatLng,
    getWebviewList: getWebviewList,
    getPhotoVideo: getPhotoVideo,
    getCompanyProfile: getCompanyProfile,
    getStoreInformation: getStoreInformation,
    getFacebook: getFacebook,
    getInstagram: getInstagram,
    getTwitter: getTwitter,
    getLayoutSetting: getLayoutSetting,
    getHistory: getHistory,
    getIntroduction: getIntroduction,
    getPhilosophy: getPhilosophy,
    getCatalogue: getCatalogue,
    getGallery: getGallery,
    getStaff: getStaff,
    getReservation: getReservation,
    addReservation: addReservation,
    cancelReservation: cancelReservation,
    sendPush: sendPush,
    sendBirthPush: sendBirthPush,
    sendWelcomePush : sendWelcomePush,
    getCoupon: getCoupon,
    getStamp: getStamp
}

module.exports = model;