var createUniqueString = function () {
    const timestamp = +new Date() + ''
    const randomNum = parseInt((1 + Math.random()) * 65536) + ''
    return (+(randomNum + timestamp)).toString(32)
}

var urlify = function (text) {
    var urlRegex = /(https?:\/\/[^\s]+)/g
    return text.replace(urlRegex, function(url) {
        return '<a href="' + url + '">' + url + '</a>'
    })
    // or alternatively
    // return text.replace(urlRegex, '<a href="$1">$1</a>')
}
  
var exports = {
    createUniqueString: createUniqueString,
    urlify: urlify
}

module.exports = exports;